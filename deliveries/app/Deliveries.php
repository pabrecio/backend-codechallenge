<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliveries extends Model
{
    protected $fillable = array('user_id', 'address', 'date_delivery','time_zone_start','time_zone_end');
}
