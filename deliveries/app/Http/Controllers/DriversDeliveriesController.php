<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DriversDeliveries as DriversDeliveriesResources;
use App\DriversDeliveries;

class DriversDeliveriesController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show_deliveries ($id)
    {
        try {
            $results = DriversDeliveries::getDeliveriesByDriver($id);

            return response()->json(new DriversDeliveriesResources($results),200);

        } catch (\Exception $e) {
            return response()->json(array("data" => $e->getMessage()),404);
        }

    }
}
