<?php

namespace App\Http\Controllers;

use App\Drivers;
use Illuminate\Http\Request;
use App\Http\Resources\Delivery as DeliveryResources;
use App\Deliveries;
use App\DriversDeliveries;
use Carbon\Carbon;

class DeliveriesController extends Controller
{
    const VALIDATE_TIME_ZONE = 8;
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request)
    {

        try {

            $this->validateTimeZone($request);

            $delivery = Deliveries::create($request->json()->all());

            $driver = Drivers::inRandomOrder()->first();

            DriversDeliveries::create([
                'drivers_id' => $driver->id,
                'deliveries_id' => $delivery->id,
                'status' => "start",
            ]);

            return response()->json(new DeliveryResources($delivery), 201);
        } catch (\Exception $e) {
            return response()->json(array("data"=>$e->getMessage()), 400);
        }

    }

    /**
     * @param $request
     * @return bool
     * @throws \Exception
     */
    private function validateTimeZone($request) {

        $startTime = Carbon::parse($request->json()->get('time_zone_start'));
        $finishTime = Carbon::parse($request->json()->get('time_zone_end'));


        if ($finishTime->diffInHours($startTime) > self::VALIDATE_TIME_ZONE) {
            throw new \Exception("Invalid time_zone");
        }

        return true;
    }
}
