<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Delivery as DeliveryResources;
use App\Http\Resources\Drivers as DriversResources;
use App\Drivers;
use App\Deliveries;

class DriverDelivery extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'DriverDelivery',
            'id'            => (string)$this->id,
            'attributes'    => [
                'driver' => new DriversResources(Drivers::find($this->drivers_id)),
                'delivery' => new DeliveryResources(Deliveries::find($this->deliveries_id)),
                'status' => $this->status,
                ],
        ];
    }
}
