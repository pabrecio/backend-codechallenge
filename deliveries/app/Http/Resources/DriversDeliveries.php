<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\DriverDelivery;

class DriversDeliveries extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => DriverDelivery::collection($this->collection),
            'relations'=> 'relationships TBD',
            'included'=> 'included TBD',
            'links' => [
                'first' => $this->resource->url(1),
                'last' => $this->resource->url($this->resource->lastPage()),
                'prev' => $this->resource->previousPageUrl(),
                'next' => $this->resource->nextPageUrl(),
                'self' => $this->resource->resolveCurrentPath()
            ],
            'meta' => [
                'current_page' => $this->resource->currentPage(),
                'from' => $this->resource->firstItem(),
                'last_page' => $this->resource->lastPage(),
                'path' => $this->resource->resolveCurrentPath(),
                'per_page' => $this->resource->perPage() ,
                'to' => $this->resource->lastItem(),
                'total' => $this->resource->total(),
            ]
        ];
    }

    public function with($request)
    {
        return [
            'links'    => [
                'self' => route('deliveries-by-driver'),
            ],
        ];
    }
}
