<?php

namespace App\Http\Resources;

use App\Users;
use Illuminate\Http\Resources\Json\Resource;

use App\Http\Resources\User as UserResource;

class Delivery extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                    'address' => $this->address,
                    'date_delivery' => $this->date_delivery,
                    'time_zone_start' => $this->time_zone_start,
                    'time_zone_end' => $this->time_zone_end,
                    'user' =>  new UserResource(Users::find($this->user_id)),
        ];
    }
}
