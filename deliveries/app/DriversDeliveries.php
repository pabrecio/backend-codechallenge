<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriversDeliveries extends Model
{
    protected $fillable = array('drivers_id', 'deliveries_id', 'status');

    const PAGINATE = 10;

    /**
     * @param $driver
     * @return mixed
     */
    public static function getDeliveriesByDriver($driver) {

        return DriversDeliveries::leftJoin('deliveries',
            function($join){
                $join->on('drivers_deliveries.deliveries_id','=','deliveries.id')
                    ->where('deliveries.date_delivery','=',date('Y-m-d'))
                ;
            })
            ->where('drivers_id','=', $driver)
            ->where('deliveries.date_delivery','=',date('Y-m-d'))
            ->orderBy('deliveries.date_delivery', 'DESC')
            ->paginate(self::PAGINATE);
    }
}
