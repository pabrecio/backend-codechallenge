<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique("email");
            $table->string('phoneNumber');
            $table->timestamps();
        });


        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('address');
            $table->date('date_delivery');
            $table->string('time_zone_start');
            $table->string('time_zone_end');
            $table->timestamps();
        });

        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('drivers_deliveries', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('drivers_id');
            $table->foreign('drivers_id')->references('id')->on('drivers');

            $table->unsignedInteger('deliveries_id');
            $table->foreign('deliveries_id')->references('id')->on('deliveries');

            $table->enum('status',array("start","go","deliveried"));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->setFKCheckOff();

        Schema::dropIfExists('users');
        Schema::dropIfExists('deliveries');
        Schema::dropIfExists('drivers');
        Schema::dropIfExists('drivers_deliveries');

        $this->setFKCheckOn();


    }

    private function setFKCheckOff() {
        switch(DB::getDriverName()) {
            case 'mysql':
                DB::statement('SET FOREIGN_KEY_CHECKS=0');
                break;
            case 'sqlite':
                DB::statement('pragma foreign_keys=0');
                break;
        }
    }

    private function setFKCheckOn() {
        switch(DB::getDriverName()) {
            case 'mysql':
                DB::statement('SET FOREIGN_KEY_CHECKS=1');
                break;
            case 'sqlite':
                DB::statement('pragma foreign_keys=1');
                break;
        }
    }
}
