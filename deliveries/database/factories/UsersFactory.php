<?php

use Faker\Generator as Faker;

$factory->define(App\Users::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'phoneNumber' => $faker->phoneNumber,
    ];
});
