<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->setFKCheckOff();

        $this->call(UsersTableSeeder::class);
        $this->call(DeliveriesTableSeeder::class);
        $this->call(DriversTableSeeder::class);
        $this->call(DriversDeliveriesTableSeeder::class);

        $this->setFKCheckOn();
        Model::reguard();
    }

    private function setFKCheckOff() {
        switch(DB::getDriverName()) {
            case 'mysql':
                DB::statement('SET FOREIGN_KEY_CHECKS=0');
                break;
            case 'sqlite':
                DB::statement('pragma foreign_keys=0');
                break;
        }
    }

    private function setFKCheckOn() {
        switch(DB::getDriverName()) {
            case 'mysql':
                DB::statement('SET FOREIGN_KEY_CHECKS=1');
                break;
            case 'sqlite':
                DB::statement('pragma foreign_keys=1');
                break;
        }
    }
}
