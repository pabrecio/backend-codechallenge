<?php

use Illuminate\Database\Seeder;

class DriversDeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\DriversDeliveries::truncate();

        \App\DriversDeliveries::create([
            'drivers_id' => 1,
            'deliveries_id' => 1,
            'status' => "start",
        ]);

        \App\DriversDeliveries::create([
            'drivers_id' => 1,
            'deliveries_id' => 2,
            'status' => "start",
        ]);

        \App\DriversDeliveries::create([
            'drivers_id' => 2,
            'deliveries_id' => 3,
            'status' => "start",
        ]);

        \App\DriversDeliveries::create([
            'drivers_id' => 2,
            'deliveries_id' => 4,
            'status' => "start",
        ]);

    }
}
