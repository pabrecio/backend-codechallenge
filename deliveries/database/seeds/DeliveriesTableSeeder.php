<?php

use Illuminate\Database\Seeder;

class DeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \App\Deliveries::truncate();


        $faker = \Faker\Factory::create();

        \App\Deliveries::create([
            'user_id' => 1,
            'address' => $faker->address,
            'date_delivery' => date('Y-m-d'),
            'time_zone_start' => "19:00",
            'time_zone_end' => "20:00"
        ]);

        \App\Deliveries::create([
            'user_id' => 1,
            'address' => $faker->address,
            'date_delivery' => $faker->date('Y-m-d'),
            'time_zone_start' => "19:00",
            'time_zone_end' => "21:00"
        ]);

        \App\Deliveries::create([
            'user_id' => 2,
            'address' => $faker->address,
            'date_delivery' => date('Y-m-d'),
            'time_zone_start' => "18:00",
            'time_zone_end' => "20:00"
        ]);

        \App\Deliveries::create([
            'user_id' => 2,
            'address' => $faker->address,
            'date_delivery' => $faker->date('Y-m-d'),
            'time_zone_start' => "15:00",
            'time_zone_end' => "20:00"
        ]);

    }
}
