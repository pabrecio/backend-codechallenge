<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     *
     * @return void
     */
    public function testDriversDeliveriesDriverTest()
    {
        $response = $this->get('api/driversDeliveries/driver/1');

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $data);

    }

    /**
     *
     * @return void
     */
    public function testDriversDeliveriesDriverNotFoundTest()
    {
        $response = $this->get('api/driversDeliveries/driver/555');

        $response->assertStatus(200);
        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $data);

        $this->assertEquals(count($data['data']),0);


    }

    /**
     *
     * @return void
     */
    public function testInsertDeliveryTest()
    {
        $response = $this->postJson('api/deliveries',["user_id"=>"1","address"=>"test","date_delivery"=>"2018-08-04", "time_zone_start"=> "12:00","time_zone_end"=> "18:00"]);

        $response->assertStatus(201);
    }

    /**
     *
     * @return void
     */
    public function testInsertDeliveryFailTest()
    {
        $response = $this->postJson('api/deliveries',["address"=>"test","date_delivery"=>"2018-08-04", "time_zone"=> "8:00-9:00"]);

        $response->assertStatus(400);
    }
}
